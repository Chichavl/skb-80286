var _ = require('underscore');
var express = require('express');
var app = express();

var pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

var pc = {"board":{"vendor":"IBM","model":"IBM-PC S-100","cpu":{"model":"80286","hz":12000},"image":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg","video":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"},"ram":{"vendor":"CTS","volume":1048576,"pins":30},"os":"MS-DOS 1.25","floppy":0,"hdd":[{"vendor":"Samsung","size":33554432,"volume":"C:"},{"vendor":"Maxtor","size":16777216,"volume":"D:"},{"vendor":"Maxtor","size":8388608,"volume":"C:"}],"monitor":null,"length":42,"height":21,"width":54};


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    res.json(pc);
});

app.get('/board', function (req, res) {
    res.json(pc.board);
});

app.get('/board/vendor', function (req, res) {
    res.json(pc.board.vendor);
});

app.get('/board/model', function (req, res) {
    res.json(pc.board.model);
});

app.get('/board/cpu', function (req, res) {
    res.json(pc.board.cpu);
});

app.get('/board/cpu/hz', function (req, res) {
    res.json(pc.board.cpu.hz);
});

app.get('/board/cpu/model', function (req, res) {
    res.json(pc.board.cpu.model);
});

app.get('/board/image', function (req, res) {
    res.json(pc.board.image);
});

app.get('/board/video', function (req, res) {
    res.json(pc.board.video);
});

app.get('/ram', function (req, res) {
    res.json(pc.ram);
});

app.get('/ram/vendor', function (req, res) {
    res.json(pc.ram.vendor);
});

app.get('/ram/volume', function (req, res) {
    res.json(pc.ram.volume);
});

app.get('/ram/pins', function (req, res) {
    res.json(pc.ram.pins);
});

app.get('/os', function (req, res) {
    res.json(pc.os);
});

app.get('/hdd', function (req, res) {
    res.json(pc.hdd);
});

app.get('/hdd/0', function (req, res) {
    res.json(pc.hdd[0]);
});

app.get('/hdd/0/vendor', function (req, res) {
    res.json(pc.hdd[0].vendor);
});

app.get('/hdd/0/size', function (req, res) {
    res.json(pc.hdd[0].size);
});

app.get('/hdd/0/volume', function (req, res) {
    res.json(pc.hdd[0].volume);
});

app.get('/hdd/1', function (req, res) {
    res.json(pc.hdd[1]);
});

app.get('/hdd/1/vendor', function (req, res) {
    res.json(pc.hdd[1].vendor);
});

app.get('/hdd/1/size', function (req, res) {
    res.json(pc.hdd[1].size);
});

app.get('/hdd/1/volume', function (req, res) {
    res.json(pc.hdd[1].volume);
});

app.get('/hdd/2', function (req, res) {
    res.json(pc.hdd[2]);
});

app.get('/hdd/2/vendor', function (req, res) {
    res.json(pc.hdd[2].vendor);
});

app.get('/hdd/2/size', function (req, res) {
    res.json(pc.hdd[2].size);
});

app.get('/hdd/2/volume', function (req, res) {
    res.json(pc.hdd[2].volume);
});

app.get('/volumes', function (req, res) {
    var obj = {};
    var groups = _.groupBy(pc.hdd, 'volume');
    var out = _.mapObject(groups, function(g, key) {
        return _(g).reduce(function(m,x) { return m + x.size; }, 0) + "B";
    });
   res.json(out);
});

app.get('/floppy', function (req, res) {
    res.json(pc.floppy);
});

app.get('/monitor', function (req, res) {
    res.json(pc.monitor);
});

app.get('/length', function (req, res) {
    res.json(pc.length);
});

app.get('/height', function (req, res) {
    res.json(pc.height);
});

app.get('/width', function (req, res) {
    res.json(pc.width);
});

app.get('*', function (req, res) {
    res.status(404).send("Not Found");
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});